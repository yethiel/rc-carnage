extends Node

var playing_vehicles = []
var results = []
var mode_ended = false

onready var countdown_timer = Timer.new()
var countdown_player = AudioStreamPlayer.new()
var countdown_state = 3


func _process(delta):
	if countdown_state < 3:
		get_node("lcs_ui/label_countdown").set_text("%d" % (countdown_state + 1))
	if countdown_state < 0:
		get_node("lcs_ui/label_countdown").set_text("")
		
	if countdown_state >= 0 and countdown_state >= countdown_timer.time_left:
		if countdown_state == 0:
			countdown_player.stream = preload("res://sfx/countdown_go.wav")
		countdown_player.play()
		countdown_state -= 1


func _ready():
	# Gets all the registered vehicles
	for vehicle in Game.get_registered_vehicles():
		playing_vehicles.append(vehicle)
		countdown_timer.connect("timeout", vehicle, "_on_countdown_finished")

	# Finds all areas that are important for the game mode
	for zone in Game.level.zones:
		if zone.type == "out_of_bounds":
			# Connects out of bound areas with own function
			zone.connect("body_entered", self, "_on_vehicle_oob")
			print_debug("Found suitable zone for LCS (oob)")
	
	# Sets up the countdown
	countdown_timer.set_wait_time(4)
	countdown_timer.set_one_shot(true)
	countdown_timer.set_autostart(true)
	add_child(countdown_timer)

	add_child(countdown_player)
	countdown_player.stream = preload("res://sfx/countdown_normal.wav")


func clear():
	playing_vehicles = []
	results = []
	mode_ended = false


func _on_vehicle_oob(veh_ref):
	if mode_ended:
		return
	print_debug(veh_ref, " went out of bounds.")
	
	# Ignores other objects and cars that already fell off
	if not veh_ref in playing_vehicles:
		return

	# Removes the car from the playing list and takes note in the results list
	playing_vehicles.erase(veh_ref)
	print_debug("Player ", veh_ref.player_id, " falls off.")
	results.insert(0, veh_ref.player_id)

	# Checks if the game is over
	if len(playing_vehicles) == 1:
		results.insert(0, playing_vehicles[0].player_id)
		_end_mode()
	else:
		print_debug(len(playing_vehicles), " players remaining")


func _end_mode():
	print_debug("Player ", playing_vehicles[0].player_id, " wins.")
	print_debug("Game over! Results: ", results)
	mode_ended = true
	
	for vehicle in Game.get_registered_vehicles():
		vehicle.disable()

	# Fills the results screen
	var results_screen = Game.get_results_screen()
	results_screen.clear()
	results_screen.set_title("Results: Last Car Standing")
	results_screen.set_column_title(0, "Place")
	for x in range(len(results)):
		results_screen.add_to_column(0, "%d" % (x+1))

	results_screen.set_column_title(1, "Name")
	for x in results:
		results_screen.add_to_column(1, "Player %d" % x)

	results_screen.set_column_title(2, "Vehicle")
	for x in results:
		results_screen.add_to_column(2, Settings.get_player_settings(x).vehicle)

	Game.show_results_screen()