tool
extends EditorScript


func _run():
	
	var W = preload("res://scripts/formats/import_w.gd").new()
	var Inf = preload("res://scripts/formats/import_inf.gd").new()
	var RVData = preload("res://scripts/rvdata.gd").new()
	var Settings = preload("res://scripts/settings.gd").new()
	var G = preload("res://scripts/global_vars.gd").new()
	var level_name = "quake"
	
	var info = {}

	var content_dir = "/home/marv/Applications/cw-carnage/content"
	
	var w_path = G.join_paths([content_dir, "levels", level_name, "%s.w" % level_name])
	var json_path = G.join_paths([content_dir, "levels", level_name, "%s.json" % level_name])
	var inf_path = G.join_paths([content_dir, "levels", level_name, "%s.inf" % level_name])
	
	
	var level_inf = Inf.import_inf(inf_path)
	var level_json
	
	if level_inf:
		info["startpos"] = level_inf["startpos"] * RVData.SCALE
		info["startrot"] = -level_inf["startrot"] * 2 * PI
	
	if G.is_file(json_path):
		level_json = read_level_json(json_path)
		print_debug("Found level JSON file")
		G.merge_dicts(info, level_json)
	
	var static_body = StaticBody.new()
	print_debug("Loading level ", level_name)
	var mesh = W.import_mesh(w_path)
	var mesh_ins = MeshInstance.new()
	mesh_ins.set_mesh(mesh)
	# Don't cast shadows since they're baked into vertex colors
	mesh_ins.cast_shadow = false
	
	var coll = CollisionShape.new()
	var shape = ConcavePolygonShape.new()
	shape.set_faces(mesh.get_faces())
	coll.set_shape(shape)
	
	static_body.add_child(mesh_ins)
	static_body.add_child(coll)
	
	get_scene().add_child(static_body)
	
#	if level_json and "zones" in level_json:
#		load_zones(level_json["zones"])
		
func read_level_json(path):
	var level_json
	var file = File.new()
	file.open(path, file.READ)
	var text = file.get_as_text()
	file.close()
	var json_error = validate_json(text)
	if json_error == "":
		level_json = parse_json(text)
	else:
		Error.message_fatal("Fatal Error: Level file is not valid :\nFile: %s\nError: %s" % [path, json_error])
	return level_json