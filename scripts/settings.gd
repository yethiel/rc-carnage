extends Node

const SETTINGS_FNAME = "settings.json"
var level
var content_dir
var game_mode

var default_settings = {
	"player0": {
		"vehicle": "gator",
	},
	"player1": {
		"vehicle": "gator",
	},
	"content_dir": get_possible_content_dirs()[0],
	"level": "lms_temple2",
	"game_mode": "lcs",
	"audio": {
		"music_enabled": true	
	},
	"video": {
		"width": -1,
		"height": -1,
		"draw_skidmarks": true,
		"light": true,
		"special_textures": true
	}
}

class PlayerSettings:
	var vehicle
	
	func as_dict():
		return {
			"vehicle": vehicle
		}
		
class VideoSettings:
	var light = true
	var draw_skidmarks = true
	var special_textures = true
	var width = -1
	var height = -1
	
	func as_dict():
		return {
			"width": width,
			"height": height,
			"light": light,
			"draw_skidmarks": draw_skidmarks,
			"special_textures": special_textures
		}

class AudioSettings:
	var music_enabled = true
	
	func as_dict():
		return {
			"music_enabled": music_enabled
		}

var player0; var player1; var player2; var player3; var player4; var player5; var player6; var player7
var video = VideoSettings.new()
var audio = AudioSettings.new()

var settings_fpath = get_settings_path()


func _ready():
	print_debug("Settings file: ", settings_fpath)
	var settings = default_settings.duplicate()
	if G.is_file(settings_fpath):
		print_debug("Settings file found")
		G.merge_dicts(settings, load_settings_file())
	else:
		print_debug("Settings file not found, creating new one")
		settings = create_settings_file()
		
	for key in settings:
		if "player" in key:
			print_debug("Creating player settings...")
			var player = PlayerSettings.new()
			set(key, player)
			for key2 in settings[key]:
				player.set(key2, settings[key][key2])
		elif "video" in key:
			for key2 in settings[key]:
				video.set(key2, settings[key][key2])
		elif "audio" in key:
			for key2 in settings[key]:
				audio.set(key2, settings[key][key2])
		else:
			set(key, settings[key])


func get_possible_content_dirs():
	var dirs = [
			OS.get_user_data_dir(),
			G.join_paths([OS.get_executable_path().get_base_dir(), "content"]),
			OS.get_executable_path().get_base_dir()
			
	]
	# 
	# if not OS.has_feature("standalone"):
	#		dirs.append("/home/marv/Projects/cw-carnage/content")
	#		dirs.append("/home/marv/Applications/cw-carnage/content")
	return dirs


""" Gets the default user directory based on OS
	The default content directory for the game on OSX is right next to the .app
	For other OSs it's right next to the binary
"""
#func get_content_dir():
#	if OS.get_name() == "OSX":
#		return G.join_paths([OS.get_executable_path().get_base_dir(), "..", "..", "content"])  # back out of MacOS/ and Contents/
#	elif OS.has_feature("standalone"):
#		return G.join_paths([OS.get_executable_path().get_base_dir(), "content"])
#	else:
#		return "/home/marv/Projects/cw-carnage/content" # editor only


""" Returns the path of the settings file.
"""
func get_settings_path():
	return G.join_paths([OS.get_user_data_dir(), "settings.json"])


func create_settings_file():
	var file = File.new()
	file.open(settings_fpath, file.WRITE)
	file.store_string(JSON.print(default_settings, "\t"))
	file.close()
	return default_settings


func save_settings_file():
	var settings = {
		"content_dir": content_dir,
		"level": level,
		"game_mode": game_mode,
	}
	
	var player
	for p in range(7):
		player = get("player%d" % p)
		if player:
			settings["player%d" % p] = player.as_dict()
	
	settings["video"] = video.as_dict()
	settings["audio"] = audio.as_dict()
	
	var file = File.new()
	file.open(settings_fpath, file.WRITE)
	file.store_string(JSON.print(settings, "\t"))
	file.close()
	return settings


func get_player_settings(player_id):
	return get("player%d" % player_id)


func load_settings_file():
	var file = File.new()
	file.open(settings_fpath, file.READ)
	if validate_json(file.get_as_text()) == "":
		var settings = parse_json(file.get_as_text())
		return settings
	else:
		Error.message("Could not read settings file. Creating new one.")
		return create_settings_file()
		
		
func set_resolution():
	if Settings.video.width != -1 and Settings.video.height != -1:
		get_viewport().set_size(Vector2(Settings.video.width, Settings.video.height))
