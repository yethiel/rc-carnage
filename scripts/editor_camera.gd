extends Camera

var mouse_lock = false
var mouse_pos = Vector2(0, 0)
var speed = 1
var yaw = 0
var pitch = 0

func _process(delta):
	if Input.is_action_pressed("editor_forward"):
		translate_object_local(Vector3(0, 0, -speed))
	if Input.is_action_pressed("editor_backward"):
		translate_object_local(Vector3(0, 0, speed))
	if Input.is_action_pressed("editor_left"):
		translate_object_local(Vector3(-speed, 0, 0))
	if Input.is_action_pressed("editor_right"):
		translate_object_local(Vector3(speed, 0, 0))
	if Input.is_action_pressed("editor_up"):
		translate_object_local(Vector3(0, speed, 0))
	if Input.is_action_pressed("editor_down"):
		translate_object_local(Vector3(0, -speed, 0))
		
func _input(event):
	if event is InputEventMouseButton and event.button_index == BUTTON_RIGHT:
		mouse_lock = event.pressed
		if mouse_lock:
			Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED | Input.MOUSE_MODE_CAPTURED)
		else:
			Input.set_mouse_mode(0)
			
	if event is InputEventMouseMotion:
		if mouse_lock and Input.is_mouse_button_pressed(2):
			yaw -= event.relative.x / 300
			pitch -= event.relative.y / 300
			pitch = clamp(pitch, -PI/2, PI/2)
			transform.basis = Basis(Vector3(0,1,0), yaw) * Basis(Vector3(1,0,0), pitch)
	
	if event is InputEventMouseButton and (event.button_index == BUTTON_WHEEL_UP or event.button_index == BUTTON_WHEEL_DOWN):
		if event.button_index == BUTTON_WHEEL_UP and speed < 10:
			speed *= 1.5
		elif event.button_index == BUTTON_WHEEL_DOWN and speed >= 0.01:
			speed /= 1.5
		mouse_pos = event.position