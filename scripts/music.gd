extends AudioStreamPlayer

var playlist = ["res://music/powercore.ogg", "res://music/helpless.ogg", "res://music/carnival.ogg"]
var current_track = 0


func _ready():
	if not Settings.audio.music_enabled:
		return
	connect("finished", self, "_on_finished")
	randomize()
	current_track = randi() % len(playlist)
	play_next()


func play_next():
	current_track += 1
	stop()
	set_stream(load(playlist[current_track % len(playlist)]))
	print_debug("Playing ", playlist[current_track % len(playlist)])
	play()


func _on_finished():
	play_next()
