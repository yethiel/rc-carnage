extends Spatial

export (NodePath) var follow_this_path = null
export var target_distance = 1.8
export var target_height = 0.6
export var laziness = 22
export var cam_id = 0

var follow_this = null
var last_lookat

#var set_up = false


func _ready():
	# Registers the camera so that a car can be assigned to it
	Game.cameras[cam_id] = self
	
	
func set_follow(node_path):
	follow_this_path = node_path
	follow_this = get_node(follow_this_path).cam_anchor
	last_lookat = follow_this.global_transform.origin + follow_this.get_transform().basis.y * 20

	
func setup():
	# move to car and adjust height
	set_translation(follow_this.global_transform.origin)
	translate(follow_this.get_transform().basis.z * 20)
	
	# TODO rotation: doesn't work right now
	#self.transform.basis.z = follow_this.get_global_transform().basis.z
	


func _physics_process(delta):
	follow_this = get_node(follow_this_path).cam_anchor
	if follow_this == null:
		return
	var delta_v = global_transform.origin - follow_this.global_transform.origin
	var target_pos = global_transform.origin
	
	# ignores y
#	delta_v.y = 0.0
	
	if (delta_v.length() > target_distance):
		delta_v = delta_v.normalized() * target_distance
		delta_v.y = target_height
		target_pos = follow_this.global_transform.origin + delta_v
	else:
		target_pos.y = follow_this.global_transform.origin.y + target_height
	
	global_transform.origin = global_transform.origin.linear_interpolate(target_pos, delta * laziness)
	last_lookat = last_lookat.linear_interpolate(follow_this.global_transform.origin, delta * laziness)
	look_at(last_lookat, Vector3(0.0, 1.0, 0.0))
