extends Node


""" Returns a list of call available car folder names """
func get_cars():
	var cars = []
	var list_cars = G.list_dir(G.join_paths([Settings.content_dir, "cars"]))
	# Checks for a parameters file to only find valid car folders
	for f in list_cars:
		if "parameters.json" in G.list_dir(G.join_paths([Settings.content_dir, "cars", f])):
			cars.append(f)
	cars.sort()
	return cars
	
	
func get_levels():
	var levels = []
	var list_levels = G.list_dir(G.join_paths([Settings.content_dir, "levels"]))
	for f in list_levels:
		if "%s.inf" % f in G.list_dir(G.join_paths([Settings.content_dir, "levels", f])):
			levels.append(f)
	levels.sort()
	return levels


func get_game_modes():
	return ["lcs", "free"]
