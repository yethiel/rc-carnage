extends Node

# Lists for easier cleaning
var car_nodes = []
var level_nodes = []

var cameras = {}
var free_camera
var current_viewport: Node

# List of device IDs that are currently in use by players
var registered_devices = []

# Dictionary (id-Player) that keeps hold of current players
var num_players = 0
var players = {}

# Data class for Player data
class Player:
	var namestr: String
	var device_id: int
	var has_vehicle = false
	var vehicle: Node

var game_mode: Node  # The current game mode node
var level: Node  # The instanced level component

"""
	Todo: split up to differentiate between a game session and player context
"""
func clear_all():
	if current_viewport:
		current_viewport.free()
	for car in car_nodes:
		car.free()
	for level in level_nodes:
		if not level == null:
			level.free()
	car_nodes = []
	level_nodes = []
	cameras.clear()
	if game_mode:
		game_mode.free()


""" Assigns the vehicle to the first player who doesn't have one yet
	At this point the vehicle does not have any properties and can thus belong to any player.
	The returned player id will be used to determine which car has to be loaded.
"""
func announce_vehicle():
	var sce = get_tree().get_current_scene()
#	var root = get_tree().get_roo
	for p in players:
		if not players[p].has_vehicle:
			players[p].has_vehicle = true
			return p


func register_vehicle(player_id, veh_ref):
	print("Cams: ", cameras)
	players[player_id].vehicle = veh_ref
	cameras[player_id].set_follow(veh_ref.get_path())
	

func register_game_mode(node):
	game_mode = node


func get_registered_vehicles():
	var vehs = []
	for p in players:
		vehs.append(players[p].vehicle)
	return vehs


""" Removes all vehicle references from player objects """
func unregister_vehicles():
	for p in players:
		players[p].has_vehicle = false
		players[p].vehicle = null


""" Returns true if the device is already bound to a player """
func device_registered(device_id):
	return device_id in registered_devices


""" Registers a player and binds it to a device id
	-1 is keyboard
	> 0 is any controller
"""
func register_player(device_id):
	# Checks if player with the same device already exists
	for p in players:
		if players[p].device_id == device_id:
			print("Player with device %d already registered" % device_id)
			return
	
	var p = Player.new()
	p.namestr = ""
	p.device_id = device_id
	registered_devices.append(device_id)
	players[num_players] = p
	print("Registered player %d for device %d" % [num_players, device_id])
	num_players += 1


""" Removes a player from the player list and unregisters their device """
func unregister_player(player_id):
	registered_devices.remove(players[player_id].device_id)
	players.erase(player_id)


func start():
	var sce = get_tree().get_root().get_node("main")
	
	clear_all()
	unregister_vehicles()
	
	var viewport
	match num_players:
		0:
			Error.message(tr("ERROR_NO_REGISTERED_PLAYERS"))
			return false
		1:
			viewport = load("res://ui/viewports/single.tscn")
		2:
			viewport = load("res://ui/viewports/split_2_vertical.tscn")
		3:
			Error.message("Number of players not yet supported.")
			return

	current_viewport = viewport.instance()
	sce.add_child(current_viewport)
	
	if not Settings.game_mode == "free":
		game_mode = load("res://modes/%s/%s_main.tscn" % [Settings.game_mode, Settings.game_mode]).instance()
	
	var vehicle_loader = load("res://components/vehicle/vehicle.tscn")
	for player in players:
		var veh = vehicle_loader.instance()
		sce.add_child(veh)
		car_nodes.append(veh)
		if Settings.game_mode == "free":
			veh.get_children()[0].enable()

	if Settings.video.light and OS.get_current_video_driver() != OS.VIDEO_DRIVER_GLES2:
		var level_old = load("res://resources/level_prototype.tscn").instance()
		sce.add_child(level_old)
		level_nodes.append(level_old)
	
	level = load("res://components/level/level.tscn").instance()
	level_nodes.append(level)
	sce.add_child(level)
	if game_mode:
		sce.add_child(game_mode)
	
	var start_grid = load("res://components/start_grid/start_grid.tscn").instance()
	sce.add_child(start_grid)
	level_nodes.append(start_grid)
	
	free_camera = load("res://components/free_camera/free_camera.tscn").instance()
	sce.add_child(free_camera)
	level_nodes.append(free_camera)
	
	setup_cameras()
	Settings.set_resolution()
	
	get_node("/root/main/canvas_layer/main_ui/loading_screen").hide()
	
	return true


func setup_cameras():
	for c in cameras:
		cameras[c].setup()


func get_results_screen():
	return get_node("/root/main/canvas_layer/main_ui/results_screen")


func show_results_screen():
	var results_screen = get_results_screen()
	results_screen.show()


func hide_results_screen():
	var results_screen = get_results_screen()
	results_screen.hide()
	
func _unhandled_input(event):
	if Input.is_action_pressed("free_camera"):
		free_camera.make_current()
	elif Input.is_action_just_pressed("camera_default"):
		cameras[0].get_children()[0].make_current()
