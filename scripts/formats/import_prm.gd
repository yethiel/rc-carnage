var FACE_QUAD = 1  # Flag for checking if a polygon is tri or quad 


""" Imports a prm mesh and returns a Godot Mesh.
	LoD is not (yet) supported.
"""
func import_mesh(model_path: String, level_path: String="", textures=[]) -> ArrayMesh:
	print_debug("Loading PRM from ", model_path)
	var mesh = ArrayMesh.new()
	var file = File.new()
	file.open(model_path, File.READ)

	read_mesh(file, mesh, level_path, textures)
	file.close()

	return mesh


""" Reads PRM mesh from file into godot mesh,
	applies texture if level_path is provided
"""
func read_mesh(file: File, mesh: ArrayMesh, level_path: String="", textures=[]) -> ArrayMesh:
	var polygon_count = file.get_16()
	var vertex_count = file.get_16()
	
	var polygons = {}  # dict: texture-index: []
	for x in range(polygon_count):
		var p_type = file.get_16()
		var p_texture = file.get_16()
	
		var vertex_indices = []
		for v in range(4):
			vertex_indices.append(file.get_16())
		
		# Reads colors: RBG and alpha
		var colors = []
		for c in range(4):
			colors.append([float(file.get_8()), float(file.get_8()), float(file.get_8()), float(file.get_8())])
		
		var uv = []
		for u in range(4):
			uv.append(Vector2(file.get_float(), file.get_float()))
		
		if not p_texture in polygons:
			polygons[p_texture] = []
		
		polygons[p_texture].append({
			"type": p_type,
			"indices": vertex_indices,
			"colors": colors,
			"uv": uv
		})

	var verts = []
	for x in range (vertex_count):
		verts.append([Vector3(-file.get_float()*RVData.SCALE, -file.get_float()*RVData.SCALE, file.get_float()*RVData.SCALE),
			Vector3(-file.get_float(), -file.get_float(), file.get_float()).normalized()])
			
	var texture_path: String
	var st = SurfaceTool.new()

	for texnum in polygons:
		st.begin(Mesh.PRIMITIVE_TRIANGLES)
		
		for p in polygons[texnum]:
			for x in [0, 1, 2]:
				st.add_uv(p["uv"][x])
				st.add_color(Color(p["colors"][x][2]/256.0, p["colors"][x][1]/256.0, p["colors"][x][0]/256.0, p["colors"][x][3]/256.0))
				st.add_normal(verts[p["indices"][x]][1])
				st.add_vertex(verts[p["indices"][x]][0])
				
			if p["type"] & FACE_QUAD:
				for x in [0, 2, 3]:
					st.add_uv(p["uv"][x])
					st.add_color(Color(p["colors"][x][2]/256.0, p["colors"][x][1]/256.0, p["colors"][x][0]/256.0, p["colors"][x][3]/256.0))
					st.add_normal(verts[p["indices"][x]][1])
					st.add_vertex(verts[p["indices"][x]][0])
			if level_path and texnum >= 0 and texnum <= 64:
				texture_path = G.join_paths([level_path, "%s%s.png" % [G.get_base_dir(level_path), RVData.int_to_texchar(texnum)]])
				if not G.is_file(texture_path):
					texture_path = G.join_paths([level_path, "%s%s.bmp" % [G.get_base_dir(level_path), RVData.int_to_texchar(texnum)]])
				st.set_material(RVData.import_tex_as_material(texture_path))
			else:
				st.set_material(RVData.no_texture_material)
			if textures and texnum >= 0 and texnum < len(textures):
				st.set_material(RVData.import_tex_as_material(G.join_paths([Settings.content_dir, textures[texnum]])))

		st.index()
		st.generate_tangents()
		st.commit(mesh)
		st.clear()
	return mesh
