extends Node

const bmp = [0x42, 0x4d]
const png = [0x89, 0x50, 0x4e, 0x47]
const jpg = [0xff, 0xd8, 0xff, 0xe0]


""" Gets the image type by reading the first bytes.
	This is necessary for files that don't use a matching file extension.
"""	
func get_image_type(path):
	var file = File.new()
	
	file.open(path, File.READ)
	
	var b1 = file.get_8()
	var b2 = file.get_8()
	var b3 = file.get_8()
	var b4 = file.get_8()
	
	file.close()
	
	if b1 == bmp[0] and b2 == bmp[1]:
		return "bmp"
	elif b1 == png[0] and b2 == png[1] and b3 == png[2] and b4 == png[3]:
		return "png"
	elif b1 == jpg[0] and b2 == jpg[1] and b3 == jpg[2] and b4 == jpg[3]:
		return "jpg"
	else:
		return "unknown"


func import_image(path):
	var format = get_image_type(path)
	var extension = path.get_extension().to_lower()
	var tex = ImageTexture.new()
	var img = Image.new()
#	prints(path, extension, format)
	if extension == format:
		img.load(path)
		
#		if format == "bmp":
#			var size = img.get_size()
#			var col
#			for x in range(size.x):
#				for y in range(size.y):
#					col = img.get_pixel(x, y)
#					if col.r == 0.0 and col.g == 0.0 and col.b == 0:
#						img.set_pixel(x, y, Color(0.0, 0.0, 0.0, 0.0))
		
	else:
		var file = File.new()
		file.open(path, File.READ)
		var buffer = file.get_buffer(file.get_len())
		match format:
			"jpg":
				img.load_jpg_from_buffer(buffer)
			"png":
				img.load_png_from_buffer(buffer)
	tex.create_from_image(img)
	return tex