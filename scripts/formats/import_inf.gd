extends Node

""" Reads RV track information files """
func import_inf(path):
	if not G.is_file(path):
		Error.message_fatal("Error loading .inf: File not found")
		return
	var inf = {}
	var f = File.new()
	f.open(path, File.READ)
	
	var line
	while not f.eof_reached():
		line = f.get_line()
		# Removes comments and converts tabs to spaces
		line = line.split(";")[0].replace("\t", " ")
		var keyval = line.split(" ", false, 1)
		if len(keyval) != 2:  # skips empty lines
			continue
		var key = keyval[0].to_lower()
		var val = keyval[1]
		val = strip_outer_whitespace(val)
		# Converts known keywords to proper format
		match key:
			"startpos":
				var coords = val.split(" ")
				
				val = Vector3(
					coords[0].to_float(),
					coords[1].to_float(),
					coords[2].to_float()
				)
				
			"startrot":
				val = val.to_float()
		inf[key] = val
	return inf


""" Strips the outter spaces of a string """
func strip_outer_whitespace(string):
	var new_string = string.trim_prefix(" ").trim_suffix(" ")
	if string != new_string:
		strip_outer_whitespace(new_string)
	return new_string
