var SCALE = 0.01  # Scale factor for import
var FACE_QUAD = 1  # Flag for checking if a polygon is tri or quad 


""" Imports a w mesh and returns a Godot Mesh.
"""
func import_mesh(model_path) -> ArrayMesh:	
	var ImportPRM = preload("res://scripts/formats/import_prm.gd").new()

	var level_path = model_path.get_base_dir()
	var file = File.new()
	file.open(model_path, File.READ)
	
	var mesh = ArrayMesh.new()
	var mesh_count = file.get_32()
	print_debug("Mesh count of w: ", mesh_count)
	
	for m in range(mesh_count):
		# bound ball center
		file.get_float()
		file.get_float()
		file.get_float()
		
		# bound ball radius
		file.get_float()
		
		#xlo, hlo
		file.get_float()
		file.get_float()
		#y
		file.get_float()
		file.get_float()
		# z
		file.get_float()
		file.get_float()
		
		ImportPRM.read_mesh(file, mesh, level_path)
		
#	var bigcube_count = file.get_32()
#	var animation_count = file.get_32()
#	var env_count = file.get_16()
	
	file.close()
	return mesh