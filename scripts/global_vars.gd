extends Node

var current_scene
var current_car


func _ready():
	var root = get_tree().get_root()
	current_scene = root.get_child(root.get_child_count() - 1)


""" Updates the first dict with the second one.
	Comparable to dict_to.update(dict_from).
"""
func merge_dicts(dict_to, dict_from):
	for key in dict_from:
		if dict_to.has(key):
			var val = dict_to[key]
			if typeof(val) == TYPE_DICTIONARY:
				merge_dicts(val, dict_from[key])
			else:
				dict_to[key] = dict_from[key]
		else:
			dict_to[key] = dict_from[key]

""" Adds all items from one array to the other """
func merge_arrays(array_to, array_from):
	for item in array_from:
		array_to.append(item)

""" Applies a material to all surfaces of a mesh """
func set_material(mesh, material):
	for x in range(mesh.get_surface_count()):
		mesh.surface_set_material(x, material)
		
func arr2vec3(list):
	return Vector3(list[0], list[1], list[2])


""" Stuff that's missing from the OS library """

""" Returns the operating system's path separator """
func get_separator():
	var os = OS.get_name()
	if os in ["Windows", "UWP"]:
		return "\\"
	else:
		return "/"


func is_dir(path):
	var dir = Directory.new()
	return dir.dir_exists(path)


func is_file(path):
	var file = File.new()
	return file.file_exists(path)


""" Partially taken from the Godot docs """
func list_dir(path):
	var out = []
	var dir = Directory.new()
	if dir.open(path) == OK:
		dir.list_dir_begin()
		var file_name = dir.get_next()
		while (file_name != ""):
			if not file_name in [".", ".."]:
				out.append(file_name)
			file_name = dir.get_next()
		return out
	else:
		print("An error occurred when trying to access the path.")
		return []


""" Splits a string using regex and returns an array """
func re_split_string(string, re):
	var out = []
	var r = RegEx.new()
	r.compile(re)
	for item in r.search_all(string):
		out.append(item.get_string())
	return out


func split_multiple(string, str1, str2):
	var temp = string.split(str1)
	var out = []
	for item in temp:
		for item2 in item.split(str2):
			out.append(item2)
	return out


""" Joins a list of paths, OS agnostic """
func join_paths(paths):
	var names = []
	var out = ""
	for p in paths:
		merge_arrays(names, split_multiple(p, "/", "\\"))  # TODO: Replace with regex
#		merge_arrays(names, re_split_string(p, "[\\\\|\\/]"))
	for n in names:
		out += n
		if out.length() == 0 and OS.get_name() in ["OSX", "X11"]:
			out += get_separator()
		elif out[-1] != get_separator() and n != names[-1]:
			out += get_separator()
	return out
	
	
""" Basedir function that actually works """
func get_base_dir(string):
	return split_multiple(string, "/", "\\")[-1]
