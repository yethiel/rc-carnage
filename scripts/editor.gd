extends Spatial

func _ready():
	var sce = get_tree().get_current_scene()
	
	var level = load("res://components/level/level.tscn").instance()
	sce.add_child(level)
	
	var level_old = load("res://resources/level_prototype.tscn").instance()
	sce.add_child(level_old)