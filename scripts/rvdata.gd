extends Node
onready var ImportTexture = preload("res://scripts/formats/import_texture.gd").new()

""" Dictionary (e.g. 'path/to/bot_bata' → Material) for loaded materials
	of legacy RV files so that they don't need to be loaded again.
"""
var loaded_materials = {}
var loaded_meshes = {}
const SCALE = 0.01
onready var no_texture_material = SpatialMaterial.new()
const SPECIAL_FLAGS = {
	"metallic": SpatialMaterial.TEXTURE_METALLIC,
	"roughness": SpatialMaterial.TEXTURE_ROUGHNESS,
	"emission": SpatialMaterial.TEXTURE_EMISSION,
	"normal": SpatialMaterial.TEXTURE_NORMAL,
	"ao": SpatialMaterial.TEXTURE_AMBIENT_OCCLUSION,
	"rim": SpatialMaterial.TEXTURE_RIM,
	"clearcoat": SpatialMaterial.TEXTURE_CLEARCOAT,
	"flowmap": SpatialMaterial.TEXTURE_FLOWMAP,
	"depth": SpatialMaterial.TEXTURE_DEPTH,
	"sss": SpatialMaterial.TEXTURE_SUBSURFACE_SCATTERING,
	"transmission": SpatialMaterial.TEXTURE_TRANSMISSION,
	"refraction": SpatialMaterial.TEXTURE_REFRACTION,
	"detail-mask": SpatialMaterial.TEXTURE_DETAIL_MASK,
	"detail-albedo": SpatialMaterial.TEXTURE_DETAIL_ALBEDO,
	"detail-normal": SpatialMaterial.TEXTURE_DETAIL_NORMAL
}

func _ready():
	no_texture_material.set_flag(SpatialMaterial.FLAG_ALBEDO_FROM_VERTEX_COLOR, true)


""" Imports a texture as a material """
func import_tex_as_material(path) -> SpatialMaterial:
	var texname = path.get_basename()
	if texname in loaded_materials:
		return loaded_materials[texname]
	if not G.is_file(path):
		print("Error: Could not find texture file: %s" % path)
		return null
	var gles2 = OS.get_current_video_driver() == OS.VIDEO_DRIVER_GLES2
	var extension = path.get_extension()
	var material = SpatialMaterial.new()
	var tex = ImportTexture.import_image(path)
	material.set_texture(SpatialMaterial.TEXTURE_ALBEDO, tex)
	if gles2:
		material.set_flag(SpatialMaterial.FLAG_UNSHADED, true)
	material.set_flag(SpatialMaterial.FLAG_ALBEDO_FROM_VERTEX_COLOR, true)
	# Sets special textures if enabled and not on gles2
	if Settings.video.special_textures and not gles2:
		var special_tex
		var special_path
		for special in SPECIAL_FLAGS:
			special_path = "%s_%s.%s" % [texname, special, extension]
			if G.is_file(special_path):
				special_tex = ImportTexture.import_image(special_path)
				if special == "normal":
					material.normal_enabled = true
				elif special == "metallic":
					material.metallic = 1.0
					material.metallic_specular = 1.0
				elif special == "ao":
					material.ao_enabled = true
				# TODO: enable for other special textures
				material.set_texture(SPECIAL_FLAGS[special], special_tex)
	loaded_materials[texname] = material
	return material
	
		
func int_to_texchar(tex_num):
	var suffix = char(tex_num % 26 + 97)
	var suffix2 = tex_num / 26
	if suffix2 > 0:
		suffix += char(suffix2 + 96)
	return suffix
		