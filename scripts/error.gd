extends Node


""" Shows an error message and allows the game to continue."""
func message(message):
	print_debug(message)	
	var root = get_tree().get_root()
	var sce = get_tree().get_current_scene()
	var error_dialog = load("res://ui/main/error_dialog.tscn")
	var ed = error_dialog.instance()
	sce.get_node("canvas_layer/main_ui").add_child(ed) 
	ed.show_message(message)
	ed.set_exclusive(true)


""" Shows an error message and pauses the game. """
func message_fatal(message):
	get_tree().paused = true
	print_debug(message)
	var root = get_tree().get_root()
	var current_scene = root.get_child(root.get_child_count() - 1)
	var error_dialog = load("res://ui/main/error_dialog.tscn")
	var ed = error_dialog.instance()
	current_scene.get_node("canvas_layer/main_ui").add_child(ed) 
	ed.show_message(message)
	ed.set_exclusive(true)
	ed.fatal = true