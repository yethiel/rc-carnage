extends Area

export (String) var type

func _process(delta):
	pass


func _ready():
	if type:
		Game.level.register_zone(self)
	else:
		print_debug("Error: Level zone without type")
		

func set_size(extents: Vector3):
	get_children()[0].get_shape().set_extents(extents)
	
#	var mesh = CubeMesh.new()
#	mesh.set_size(extents * 2)
#	var inst = MeshInstance.new()
#	inst.set_mesh(mesh)
#	add_child(inst)