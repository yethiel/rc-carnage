extends Spatial


func _ready():
	start_grid_legacy()


""" Places cars in a start grid for legacy tracks (.inf) """
func start_grid_legacy():
	var level = Game.level
	
	if not level:
		OS.alert("Error: Start grid loaded before level")
		return
		
	var start_pos = level.info["startpos"]
	var start_rot = level.info["startrot"]
	
	if "start_grid" in level.info:
		for p in Game.players:
			Game.players[p].vehicle.set_translation(G.arr2vec3(start_pos) + G.arr2vec3(level.info["start_grid"][p]["position"]))
			Game.players[p].vehicle.rotate_y(level.info["start_grid"][p]["rotation"] * PI * 2)
	else:
		for p in Game.players:
			Game.players[p].vehicle.set_translation(start_pos)
			Game.players[p].vehicle.rotate_y(start_rot)