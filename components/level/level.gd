extends Spatial

onready var W = preload("res://scripts/formats/import_w.gd").new()
onready var Inf = preload("res://scripts/formats/import_inf.gd").new()

onready var info = {
	"name": "",
	"startpos": Vector3(0, 0, 0),
	"zones": []
}

var zones = []

func clear():
	zones = []

func _ready():
	var w_path = G.join_paths([Settings.content_dir, "levels", Settings.level, "%s.w" % Settings.level])
	var json_path = G.join_paths([Settings.content_dir, "levels", Settings.level, "%s.json" % Settings.level])
	var inf_path = G.join_paths([Settings.content_dir, "levels", Settings.level, "%s.inf" % Settings.level])
	
	var level_inf = Inf.import_inf(inf_path)
	var level_json
	
	if level_inf:
		info["startpos"] = level_inf["startpos"] * Vector3(-1, -1, 1) * RVData.SCALE
		info["startrot"] = -level_inf["startrot"] * 2 * PI
	
	if G.is_file(json_path):
		level_json = read_level_json(json_path)
		print_debug("Found level JSON file")
		G.merge_dicts(info, level_json)
	
	var static_body = StaticBody.new()
	print_debug("Loading level ", Settings.level)
	var mesh
	if w_path in RVData.loaded_meshes:
		mesh = RVData.loaded_meshes[w_path]
	else:
		mesh = W.import_mesh(w_path)
		RVData.loaded_meshes[w_path] = mesh
	var mesh_ins = MeshInstance.new()
	mesh_ins.set_mesh(mesh)
	# Don't cast shadows since they're baked into vertex colors
	mesh_ins.cast_shadow = false
	
	var coll = CollisionShape.new()
	var shape = ConcavePolygonShape.new()
	shape.set_faces(mesh.get_faces())
	coll.set_shape(shape)
	
	static_body.add_child(mesh_ins)
	static_body.add_child(coll)
	
	add_child(static_body)
	
	if level_json and "zones" in level_json:
		load_zones(level_json["zones"])
	
	
func read_level_json(path):
	var level_json
	var file = File.new()
	file.open(path, file.READ)
	var text = file.get_as_text()
	file.close()
	var json_error = validate_json(text)
	if json_error == "":
		level_json = parse_json(text)
	else:
		Error.message_fatal("Fatal Error: Level file is not valid :\nFile: %s\nError: %s" % [path, json_error])
	return level_json
	

func load_zones(zones):
	var level_zone = load("res://components/level_zone/level_zone.tscn")
	for zone in zones:
		var new_zone = level_zone.instance()
		new_zone.type = zone["type"]
		new_zone.set_size(G.arr2vec3(zone["size"]))
		new_zone.translate(G.arr2vec3(zone["position"]))
		add_child(new_zone)


func register_zone(area_ref):
	zones.append(area_ref)
	print_debug("Registered level zone of type ", area_ref.type)
