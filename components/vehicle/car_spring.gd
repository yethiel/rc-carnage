extends Spatial

var target
var vehicle_body
var wheel_position
var target_offset
var length = 4.0
var target_pos

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func _process(delta):
	var wheel_transform = Transform(vehicle_body.global_transform.basis, target.global_transform.origin)
	

	target_pos = wheel_transform.xform(target_offset)
	look_at(target_pos, Vector3(0, 1, 0))
	set_scale(Vector3(
			1,
			1,
			length * get_global_transform().origin.distance_to(target_pos))
	)

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
