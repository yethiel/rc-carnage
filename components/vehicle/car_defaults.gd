extends Node

var parameters = {
    "name": "Unknown",
    "author": "Unknown",
    "description": "",
    "date": "2019-06-28",
    "revision": 0,
    "type": "vehicle/car",

    "engine_force": 20,
    "steer_angle": 0.22,
    "steer_speed": 1.0,
	"top_speed": 999.0,
	"brake_strength": 2.0,
	"jump_strength": 0.0,
	"drift_force": 0.0,

	"camera": {
		"anchor": [0, 0, 0]
	},

    "body": {
        "mesh": "",
		"collision": "",
        "texture": "",
        "mass": 8.62,
		"friction": 0.5,
        "gravity_scale": 1.0,
        "offset": [0.0, 0.0, 0.0],
		"collision_offset": [0.0, 0.0, 0.0],
        "rotation": [0.0, 0.0, 0.0],
        "scale": [1.0, 1.0, 1.0],
		"angular_damp_ground": 0.8,
		"angular_damp_air": 0.8
    },

    "wheels": [],
    "objects": [],
	"springs": [],
	"axles": []
}


var wheel = {
    "name": "wheel",
    "mesh": "",
	"collision": "",
    "textures": [],
    "position": [0, 0.0, 0.0],
    "offset": [0.0, 0.0, 0.0],
    "scale": [1.0, 1.0, 1.0],
    "steer": false,
    "powered": true,
    "radius": 0.12,
	"skid_width": 0.1,
    "rest_length": 0.06,
    "grip": 1.5,
	"grip_drift": 1.5,
    "roll_influence": 0.1,
    
    "suspension_travel": 0.83,
    "suspension_stiffness": 40.47,
    "suspension_force": 6000.0,

    "damping_compression": 0.83,
    "damping_relaxation": 0.52
}

var spring = {
	"name": "",
	"mesh": "",
	"textures": [],
	"origin": [0, 0, 0],
	"target_wheel": 0
}

var axle = {
	"name": "",
	"mesh": "",
	"textures": [],
	"origin_wheel": 0,
	"target": [0, 0, 0],
	"target_offset": [0, 0, 0],
	"length": 5.0
}

var object = {
    "name": "key",
    "mesh": "",
    "texture": "",
    "position": [0.0, 0.0, 0.0],
    "scale": [1.0, 1.0, 1.0],
    "rotation": [0.0, 0.0, 0.0],
    "actions": [],
    "action_axis": {}
}