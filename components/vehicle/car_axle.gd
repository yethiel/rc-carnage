extends Spatial

var target_body
var target_offset
var origin_wheel


func _process(delta):
	set_translation(origin_wheel.get_translation())
	look_at(target_body.get_global_transform().xform(target_offset), Vector3(0, 1, 0))
#	pass
