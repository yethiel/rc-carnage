extends VehicleBody

export(float) var max_engine_force
export(float) var max_steer_angle
export(float) var steer_speed
export(int) var player_id
export(int) var controller_id

# parameters used in _physics_process
var steer_angle = 0.0
var steer_target = 0.0
var top_speed = 0.0
var brake_strength = 0.0
var angular_damp_ground = 0.9
var angular_damp_air = 0.8
var jump_strength = 1.0
var drift_force = 0.0

var RUMBLE_STRENGTH = 0.3

var parameters = {}

# "current" values
var current_speed_mps = 0.0
var current_grip
var steer_val
var throttle_val
var stunt_x_val
var stunt_y_val
var collisions
var currently_drifting = false

var do_flip = false
var last_up  # for flipping the car interpolation

# speed and locations
var rpm = 0.0
var velocity_z = 0.0
var last_pos = Vector3(0.0, 0.0, 0.0)

# wheels and skidmarks
var wheels = []
var wheels_last_dirs = []
var wheel_dirs = []
var skidmark_list = []
var wheel_skid_status = []
onready var skidmark = ImmediateGeometry.new()
var skidmark_delta = 0

var springs = []

var cam_anchor
var cam_anchor_last  # for interpolation
var engine_sound

var enabled = false  # whether the car is allowed to move


func enable():
	axis_lock_linear_x = false
	axis_lock_linear_z = false
	axis_lock_angular_y = false
	enabled = true


func disable():
	axis_lock_linear_x = true
	axis_lock_linear_z = true
	axis_lock_angular_y = true
	enabled = false


func _ready():
	var sce = get_tree().get_current_scene()
	set_contact_monitor(true)
	set_max_contacts_reported(5)

	for node in get_children():
		if node is VehicleWheel:
#			wheels.append(node)
			skidmark_list.append([[]])
			wheel_skid_status.append(0)
	
	skidmark.set_material_override(load("res://gfx/materials/skidmark.material"))
	sce.add_child(skidmark)


func _process(delta):
	if Settings.video.draw_skidmarks:
		render_skidmarks()


func flip(delta):
	# TODO: Raise vehicle from ground and ensure enough space
	var last_last_up = last_up
	last_up = last_up.linear_interpolate(Vector3(0, 1, 0), delta*3)
	# Checks if the flip interpolation is still going on
	if abs(last_up.x - last_last_up.x) > 0.002 \
			or abs(last_up.y - last_last_up.y) > 0.002 \
			or abs(last_up.z - last_last_up.z) > 0.002:
		var transl = get_translation()
		var dir_z = get_transform().basis.z
		var look = Vector3(transl.x + 100 * -dir_z.x, transl.y, transl.z + 100 * -dir_z.z)
		look_at(look, last_up)
	else:
		do_flip = false
		set_linear_velocity(Vector3(0, 0, 0))
		set_angular_velocity(Vector3(0, 0, 0))


func start_flip():
	if get_transform().basis.y.y < 0.3:
		last_up = get_transform().basis.y
		do_flip = true


func get_speed_kph():
	return current_speed_mps * 3600.0 / 1000.0


func set_player_id(pid):
	var sce = get_tree().get_current_scene()
	player_id = pid
	controller_id = Game.players[player_id].device_id


func _physics_process(delta):
	
	velocity_z = get_linear_velocity().dot(get_transform().basis.z)
	current_speed_mps = (translation - last_pos).length() / delta
	collisions = get_colliding_bodies()

	# Gets analog values for steering and throttle, also works for keyboard
	steer_val = Input.get_action_strength("vehicle_left_%d" % controller_id) - Input.get_action_strength("vehicle_right_%d" % controller_id)
	throttle_val = Input.get_action_strength("vehicle_accelerate_%d" % controller_id) - Input.get_action_strength("vehicle_brake_%d" % controller_id)
	stunt_x_val = Input.get_action_strength("vehicle_stunt_down_%d" % controller_id) - Input.get_action_strength("vehicle_stunt_up_%d" % controller_id)
	stunt_y_val = Input.get_action_strength("vehicle_stunt_left_%d" % controller_id) - Input.get_action_strength("vehicle_stunt_right_%d" % controller_id)
	
	# Keeps track of wheel directions for RPM calculation
	wheel_dirs = []
	for wheel in wheels:
		wheel_dirs.append(wheel.get_transform().basis.z)
#		wheel_dirs.append(get_linear_velocity().normalized())
	if not wheels_last_dirs:
		wheels_last_dirs = wheel_dirs
		return
	rpm = 0.0
	for x in range(len(wheels)):
		rpm += abs(wheel_dirs[x].angle_to(wheels_last_dirs[x]))
	rpm /= 4
	# Sets the engine sound pitch
	if rpm > 0.1:
		engine_sound.set_pitch_scale(rpm)
	else:
		engine_sound.set_pitch_scale(0.1)
	wheels_last_dirs = wheel_dirs

	# Vehicle flipping
	if Input.is_action_just_pressed("vehicle_flip_%d" % controller_id):
		start_flip()
	if do_flip:
		flip(delta)

	# Skid vibration
	current_grip = 0;
	for w in range(len(wheels)):
		current_grip += wheels[w].get_skidinfo()
		if Settings.video.draw_skidmarks:
#			generate_skidmarks(w, wheels[w].get_skidinfo())
			generate_skidmarks(w, current_grip)
		
	current_grip /= 4
	if Input.get_joy_vibration_strength(controller_id).x > 0 and current_grip == 0:
		Input.stop_joy_vibration(controller_id)
	else:
		Input.start_joy_vibration(controller_id, pow((1-current_grip), 2) * RUMBLE_STRENGTH, 0, 0.05)
	
	# Stops the car from moving if not enabled after countdown
	if not enabled:
		return
	
	# Checks for top speed
	if get_speed_kph() < top_speed:
		engine_force = throttle_val * max_engine_force
	else:
		engine_force = 0
	
	if throttle_val < 0 and velocity_z > 0.1:
		engine_force *= brake_strength
	elif throttle_val > 0 and velocity_z < 0.1:
		engine_force *= brake_strength
	
	if throttle_val == 0:	
		set_brake(0.03)
	else: 
		set_brake(0)

	# Steering
	steer_target = steer_val * max_steer_angle
	if (steer_target < steer_angle):
		steer_angle -= steer_speed * delta
		if (steer_target > steer_angle):
			steer_angle = steer_target
	elif (steer_target > steer_angle):
		steer_angle += steer_speed * delta
		if (steer_target < steer_angle):
			steer_angle = steer_target

	steering = steer_angle
	last_pos = translation
	
	# Everything that happens on ground
	if is_on_ground():
		set_angular_damp(angular_damp_ground)
		
		# Jumping
		if Input.is_action_pressed("vehicle_jump_%d" % controller_id):
			for wheel in wheels:
				apply_impulse(wheel.translation, get_transform().basis.y * jump_strength)
		
		# Drifting
		if Input.is_action_just_pressed("vehicle_drift_%d" % controller_id) and steer_val != 0:
			currently_drifting = true
			for x in range(len(wheels)):
				wheels[x].set_friction_slip(parameters["wheels"][x]["grip_drift"])
			# Applies a little torque to init the drift
			apply_torque_impulse(get_transform().basis.y * steer_val * 4)
		
		# Adds a force to keep the vehicle drifting
		if currently_drifting and get_speed_kph() < top_speed:
			add_force(get_transform().basis.z * drift_force * (1-current_grip) * throttle_val, Vector3(0, 0, 0))
		
		# Stops drifting if there's not enough skidding
		if current_grip > 0.7:
			currently_drifting = false
	else:
		set_angular_damp(angular_damp_air)  # sets angular damping for air
	
	# Stops drifting
	if Input.is_action_just_released("vehicle_drift_%d" % controller_id):
			currently_drifting = false
	
	# Resets the grip when not drifting
	if not currently_drifting:
		for x in range(len(wheels)):
				wheels[x].set_friction_slip(parameters["wheels"][x]["grip"])
	
	# Air behavior (stunts)
	if not cam_anchor_last:
		cam_anchor_last = G.arr2vec3(parameters["camera"]["anchor"])  # for interpolating the camera anchor
	if not is_on_ground() and not collisions:
		if controller_id == -1 and Input.is_action_pressed("vehicle_stunt_enable_-1"):
			stunt_y_val = steer_val
			stunt_x_val = throttle_val
		add_torque(Vector3(get_transform().basis.x.x * stunt_x_val * 30, get_transform().basis.x.y * stunt_x_val * 30, get_transform().basis.x.z * stunt_x_val * 30))
		add_torque(Vector3(get_transform().basis.y.x * stunt_y_val * 30, get_transform().basis.y.y * stunt_y_val * 30, get_transform().basis.y.z * stunt_y_val * 30))
		cam_anchor_last = cam_anchor_last.linear_interpolate(Vector3(0, 0, 0), delta * 4)
		cam_anchor.set_translation(cam_anchor_last)
	else:
		cam_anchor_last = cam_anchor_last.linear_interpolate(G.arr2vec3(parameters["camera"]["anchor"]), delta * 2)
		cam_anchor.set_translation(cam_anchor_last)


func generate_skidmarks(wheel_num, current_grip):	
	# Wheel is skidding
	if current_grip < 0.5:
		# Wheel was not previously skidding
		if not wheel_skid_status[wheel_num] == 1:
			wheel_skid_status[wheel_num] = 1
			skidmark_list[wheel_num].append([])  # Adds a new list for a new skidmark
		if skidmark_delta < 6 and skidmark_list[wheel_num][-1]:
			skidmark_delta += 1
			skidmark_list[wheel_num][-1][-1] = [
				wheels[wheel_num].get_global_transform().origin,
				wheels[wheel_num].get_global_transform().basis.x
			]
			return
		skidmark_delta = 0
		# Makes sure single skidmarks aren't too long
		if len(skidmark_list[wheel_num][-1]) > 80:
			skidmark_list[wheel_num][-1].pop_front()
		# Adds coordinates and basis of skidmark
		skidmark_list[wheel_num][-1].append([
			wheels[wheel_num].get_global_transform().origin,
			wheels[wheel_num].get_global_transform().basis.x
		])
	else:
		wheel_skid_status[wheel_num] = 0
	# Makes sure there aren't mode than 10 marks per car
	if len(skidmark_list[wheel_num]) > 10:
		skidmark_list[wheel_num].pop_front()

var cur_whl_width
var height_offset
var mark
var point1
var point2
var skid_length
var skid_uv

func render_skidmarks():
	skidmark.clear()
	for m in range(len(skidmark_list)):
		cur_whl_width = wheels[m].skid_width
		height_offset = wheels[m].get_radius() - 0.05
		mark = skidmark_list[m]
		for pointlist in mark:
			skid_length = len(pointlist)
			skidmark.begin(Mesh.PRIMITIVE_TRIANGLE_STRIP)
			for x in range(skid_length):
				skid_uv = float(x) / float(skid_length)
				point1 = Vector3(
					pointlist[x][0].x - pointlist[x][1].x * cur_whl_width, 
					pointlist[x][0].y - pointlist[x][1].y * cur_whl_width - height_offset, 
					pointlist[x][0].z - pointlist[x][1].z * cur_whl_width
				)
				point2 = Vector3(
					pointlist[x][0].x + pointlist[x][1].x * cur_whl_width, 
					pointlist[x][0].y + pointlist[x][1].y * cur_whl_width - height_offset, 
					pointlist[x][0].z + pointlist[x][1].z * cur_whl_width
				)
				skidmark.set_uv(Vector2(0.0, skid_uv))
				skidmark.add_vertex(point1)
				skidmark.set_uv(Vector2(1.0, skid_uv))
				skidmark.add_vertex(point2)
			skidmark.end()
			

func is_on_ground():
	for w in wheels:
		if not w.is_in_contact():
			return false
	return true
	

func _on_countdown_finished():
	enable()
