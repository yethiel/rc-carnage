extends Node

const CAR_SCRIPT = "res://components/vehicle/car.gd"
const WHEEL_SCRIPT = "res://components/vehicle/car_wheel.gd"
const CAR_OBJ_SCRIPT = "res://components/vehicle/car_object.gd"
onready var ImportPRM = preload("res://scripts/formats/import_prm.gd").new()
onready var ImportTexture = preload("res://scripts/formats/import_texture.gd").new()
onready var Defaults = preload("res://components/vehicle/car_defaults.gd").new()

onready var player_id = Game.announce_vehicle()


"""
Loads the vehicle as soon as node is placed.
"""
func _ready():
	load_vehicle(Settings.get_player_settings(player_id).vehicle)


func _process(delta):
	# Reload vehicle
	if Input.is_action_just_released("car_reload"):
		reload_vehicle()


""" Loads a vehicle specified in the game settings
	and distinguishes between different types.
	Currently only 'vehicle/car' is supported.
"""
func load_vehicle(vehicle_name):
	var sce = get_tree().get_current_scene()
	print_debug("Reading parameters")
	var parameters = Defaults.parameters.duplicate()
	G.merge_dicts(parameters, read_parameters(vehicle_name))
	if not parameters:
		return
	if parameters["type"] == "vehicle/car":
		var car = load_car(parameters)
		car.disable()
		car.set_player_id(player_id)
		Game.register_vehicle(player_id, car)
		G.current_car = car
		return car
		
		
func reload_vehicle():
	print_debug("reloading car")
	var old_transform
	for node in get_children():
		old_transform = node.get_global_transform()
		node.queue_free()
	var veh = load_vehicle(Settings.get_player_settings(0).vehicle)
	veh.set_player_id(player_id)
	Game.register_vehicle(player_id, veh)
	if veh and old_transform:
		veh.set_global_transform(old_transform)
	veh.enable()


""" Loads a car into the scene from Re-Volt style parameters
	Uses Godot's built-in VehicleBody.
"""
func load_car(parameters):
	var v_body = VehicleBody.new()
	v_body.set_script(load(CAR_SCRIPT))
	v_body.parameters = parameters
	
	var body_params = parameters["body"]
	G.merge_dicts(body_params, parameters["body"])
	
	var body_material = SpatialMaterial.new()
	if body_params["mesh"]:
		var mesh
		var mesh_path = G.join_paths([Settings.content_dir, body_params["mesh"]])
		if mesh in RVData.loaded_meshes:
			mesh = RVData.loaded_meshes[mesh_path]
		else:
			mesh = ImportPRM.import_mesh(mesh_path, "", body_params["textures"])
			RVData.loaded_meshes[mesh_path] = mesh
		# Sets material
		if body_params["texture"]:
			var tex = ImportTexture.import_image(G.join_paths([Settings.content_dir, body_params["texture"]]))
			body_material.set_texture(SpatialMaterial.TEXTURE_ALBEDO, tex)
			body_material.set_roughness(0.2)
			G.set_material(mesh, body_material)		
		var mesh_ins = MeshInstance.new()
		mesh_ins.set_mesh(mesh)
		mesh_ins.set_rotation(G.arr2vec3(body_params["rotation"]))
		mesh_ins.translate(G.arr2vec3(body_params["offset"]))
		mesh_ins.set_scale(G.arr2vec3(body_params["scale"]))
		
#		var testm = load("res://scenes/mesh_instance.tscn").instance()
#		v_body.add_child(testm)
		v_body.add_child(mesh_ins)
		
		# Vehicle collision
		var coll = CollisionShape.new()
		var shape = ConvexPolygonShape.new()
		var coll_mesh = mesh
		if body_params["collision"]:
			coll_mesh = ImportPRM.import_mesh(G.join_paths([Settings.content_dir, body_params["collision"]]))
		shape.set_points(coll_mesh.get_faces())
		coll.set_shape(shape)
		coll.translate(G.arr2vec3(body_params["collision_offset"]))
		v_body.add_child(coll)
		
	# Body properties
	v_body.set_mass(body_params["mass"])
	v_body.set_friction(body_params["friction"])
	v_body.set_gravity_scale(body_params["gravity_scale"])
	v_body.set("angular_damp_ground", body_params["angular_damp_ground"])
	v_body.set("angular_damp_air", body_params["angular_damp_air"])
	
	v_body.set("max_engine_force", parameters["engine_force"])
	v_body.set("steer_speed", parameters["steer_speed"])
	v_body.set("max_steer_angle", parameters["steer_angle"])
	v_body.set("top_speed", parameters["top_speed"])
	v_body.set("brake_strength", parameters["brake_strength"])
	v_body.set("jump_strength", parameters["jump_strength"])
	v_body.set("drift_force", parameters["drift_force"])
	
	# Camera Anchor
	var cam_anchor = Spatial.new()
	cam_anchor.translate(G.arr2vec3(parameters["camera"]["anchor"]))
	v_body.add_child(cam_anchor)
	v_body.cam_anchor = cam_anchor
	
	var wheel_materials = {}
	
	# Creates wheels
	for wp in parameters["wheels"]:
		var w_params = Defaults.wheel.duplicate()
		G.merge_dicts(w_params, wp)
		print("Adding wheel")
		var wheel = VehicleWheel.new()
		wheel.set_script(load(WHEEL_SCRIPT))
		
		# Wheel mesh
		if w_params["mesh"]:
			var w_mesh_path = G.join_paths([Settings.content_dir, w_params["mesh"]])
			var w_mesh
			if w_mesh_path in RVData.loaded_meshes:
				w_mesh = RVData.loaded_meshes[w_mesh_path]
			else:
				w_mesh = ImportPRM.import_mesh(w_mesh_path, "", w_params["textures"])
				RVData.loaded_meshes[w_mesh_path] = w_mesh
			var w_mesh_ins = MeshInstance.new()
			w_mesh_ins.set_mesh(w_mesh)
			if "scale" in w_params:
				w_mesh_ins.set_scale(G.arr2vec3(w_params["scale"]))
			if OS.get_current_video_driver() == 1:
				# No idea why this happens but with GLES2 the wheels are flipped
				w_mesh_ins.set_scale(Vector3(w_mesh_ins.get_scale().x, w_mesh_ins.get_scale().y * -1, w_mesh_ins.get_scale().z))
			if "offset" in w_params:
				w_mesh_ins.set_translation(G.arr2vec3(w_params["offset"]))
			wheel.add_child(w_mesh_ins)
		
		# Wheel properties
		wheel.set_translation(G.arr2vec3(w_params["position"]))
		wheel.set_use_as_steering(bool(w_params["steer"]))
		wheel.set_use_as_traction(bool(w_params["powered"]))
		wheel.set_friction_slip(w_params["grip"])
		wheel.set_radius(w_params["radius"])
		wheel.skid_width = w_params["skid_width"]
		wheel.set_suspension_rest_length(w_params["rest_length"])
		wheel.set_roll_influence(w_params["roll_influence"])
		wheel.set_damping_compression(w_params["damping_compression"])
		wheel.set_damping_relaxation(w_params["damping_relaxation"])
		wheel.set_suspension_max_force(w_params["suspension_force"])
		wheel.set_suspension_stiffness(w_params["suspension_stiffness"])
		wheel.set_suspension_travel(w_params["suspension_travel"])
		
		v_body.add_child(wheel)
		v_body.wheels.append(wheel)
		
	for sp in parameters["springs"]:
		var spring_params = Defaults.spring.duplicate()
		G.merge_dicts(spring_params, sp)
		
		var s_mesh_path = G.join_paths([Settings.content_dir, spring_params["mesh"]])
		var s_mesh
		if s_mesh_path in RVData.loaded_meshes:
			s_mesh = RVData.loaded_meshes[s_mesh_path]
		else:
			s_mesh = ImportPRM.import_mesh(s_mesh_path, "", spring_params["textures"])
			RVData.loaded_meshes[s_mesh_path] = s_mesh
		var s_mesh_ins = MeshInstance.new()
		s_mesh_ins.set_mesh(s_mesh)
		s_mesh_ins.set_translation(G.arr2vec3(spring_params["origin"]))
		s_mesh_ins.set_script(load("res://components/vehicle/car_spring.gd"))

		s_mesh_ins.target = v_body.wheels[spring_params["target_wheel"]]
		s_mesh_ins.length = spring_params["length"]
		s_mesh_ins.target_offset = G.arr2vec3(spring_params["target_offset"])
		s_mesh_ins.vehicle_body = v_body
		s_mesh_ins.wheel_position = G.arr2vec3(parameters["wheels"][spring_params["target_wheel"]]["position"])

		v_body.add_child(s_mesh_ins)
		v_body.springs.append(s_mesh_ins)
		
	for ap in parameters["axles"]:
		var axle_params = Defaults.axle.duplicate()
		G.merge_dicts(axle_params, ap)
		var a_mesh
		var a_mesh_path = G.join_paths([Settings.content_dir, axle_params["mesh"]])
		if a_mesh_path in RVData.loaded_meshes:
			a_mesh = RVData.loaded_meshes[a_mesh_path]
		else:
			a_mesh = ImportPRM.import_mesh(a_mesh_path, "", axle_params["textures"])
			RVData.loaded_meshes[a_mesh_path] = a_mesh
		var a_mesh_ins = MeshInstance.new()
		a_mesh_ins.set_mesh(a_mesh)
#		a_mesh_ins.set_translation(G.arr2vec3(parameters["wheels"][axle_params["origin_wheel"]]["position"]))
		a_mesh_ins.set_translation(G.arr2vec3(axle_params["target"]))
		a_mesh_ins.set_script(load("res://components/vehicle/car_axle.gd"))

		a_mesh_ins.origin_wheel = v_body.wheels[axle_params["origin_wheel"]]
		a_mesh_ins.target_body = v_body
		a_mesh_ins.target_offset = G.arr2vec3(axle_params["target"])

		v_body.add_child(a_mesh_ins)
#		v_body.springs.append(a_mesh_ins)
		
	for op in parameters["objects"]:
		var o_params = Defaults.object.duplicate()
		G.merge_dicts(o_params, op)
		var o_mesh = load_any_mesh(o_params["mesh"])
		
		# Checks if object and body share a texture or loads a new one if specified
		if o_params["texture"] == body_params["texture"]:
			G.set_material(o_mesh, body_material)
		elif o_params["texture"]:
			var object_material = SpatialMaterial.new()
			var tex = ImportTexture.import_image(G.join_paths([Settings.content_dir, o_params["texture"]]))
			object_material.set_texture(SpatialMaterial.TEXTURE_ALBEDO, tex)
			G.set_material(o_mesh, object_material)
		
		var o_mesh_ins = MeshInstance.new()
		o_mesh_ins.set_mesh(o_mesh)
		o_mesh_ins.set_script(load(CAR_OBJ_SCRIPT))
		o_mesh_ins.set_translation(G.arr2vec3(o_params["position"]))
		o_mesh_ins.set_scale(G.arr2vec3(o_params["scale"]))
		o_mesh_ins.set_rotation_degrees(G.arr2vec3(o_params["rotation"]))
		o_mesh_ins.set("actions", o_params["actions"])
		o_mesh_ins.set("action_axis", o_params["action_axis"])
		v_body.add_child(o_mesh_ins)
	
	
#	var audio_file = "res://sfx/engine.wav"
#	if not G.is_file(audio_file):
#         printerr("Could not find audio file: " + audio_file)
#         return
#
#	var file = File.new()
#	if file.open(audio_file, File.READ) != OK:
#		printerr("Could not open audio file: " + audio_file)
#		return
#
#	var buffer = file.get_buffer(file.get_len())
#	file.close()
#
#	var engine_wav = AudioStreamSample.new()
#	engine_wav.format = AudioStreamSample.FORMAT_16_BITS
#	engine_wav.data = buffer
#	engine_wav.stereo = false
#	engine_wav.set_loop_mode(1)
#	engine_wav.set_loop_begin(0)
#	engine_wav.set_loop_end(0)
	
	var engine_wav = load("res://sfx/engine.wav")
	var engine_sound = AudioStreamPlayer3D.new()
	v_body.add_child(engine_sound)
	v_body.engine_sound = engine_sound
	engine_sound.set_stream(engine_wav)
	engine_sound.autoplay = true
		
	add_child(v_body)
	return v_body


""" Checks for the file type (file ending) and decides how to load
	the mesh.
	So far only PRM and .mesh (or anything Godot likes to load) is supported
"""
func load_any_mesh(fpath):
	var mesh
	if ".prm" in fpath:
		var model_path = G.join_paths([Settings.content_dir, fpath])
		mesh = ImportPRM.import_mesh(model_path)
	else:
		mesh = load(fpath)
	return mesh


""" Loads parameters from parameters.json
	Returns a dictionary.
"""
func read_parameters(vehicle_name):
	var parameters = Defaults.parameters
	var file = File.new()
	var param_path = G.join_paths([Settings.content_dir, "cars/%s/parameters.json" % vehicle_name])
	if not G.is_file(param_path):
		Error.message_fatal("Fatal Error: Could not find %s" % vehicle_name)
		return
	
	file.open(param_path, file.READ)
	var text = file.get_as_text()
	file.close()
	var json_error = validate_json(text)
	if json_error == "":
		G.merge_dicts(parameters, parse_json(text))
	else:
		Error.message_fatal("Fatal Error: Parameters file is not valid :\nFile: %s\nError: %s" % [param_path, json_error])
	return parameters
