"""
	Car Objects are like Spinners in Re-Volt. They're attached to the car and perform an action
	defined in the parameters file.
	Currently supported: rotate
"""
extends Spatial

export var actions = []
export var action_axis = {}

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


func _process(delta):
	if "rotate" in actions:
		rotate_x(action_axis["rotate"][0] * delta)
		rotate_y(action_axis["rotate"][1] * delta)
		rotate_z(action_axis["rotate"][2] * delta)
