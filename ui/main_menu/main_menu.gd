extends Node


onready var l_num_players = find_node("num_players")
onready var userpath_select_custom_button = find_node("userpath_select_custom_button")
onready var file_dialog = find_node("file_dialog")
onready var userpath_select = find_node("userpath_select")
onready var game_mode_select = find_node("game_mode_select")
onready var button_start = find_node("button_start")
onready var level_select = find_node("level_select")
onready var veh_selects = [
			find_node("vehicle_select_player0"),
			find_node("vehicle_select_player1")
		]
onready var label_userpath = find_node("label_userpath")


var visible = true

func _ready():
	connect_signals()
	init_menu()


func connect_signals():
	file_dialog.connect("dir_selected", self, "_file_dialog_on_dir_selected")
	userpath_select_custom_button.connect("button_up", self, "_userpath_select_custom_button_pressed")
	game_mode_select.get_popup().connect("id_pressed", self, "_game_mode_select_on_item_pressed")
	userpath_select.get_popup().connect("id_pressed", self, "_userpath_select_on_item_pressed")
	level_select.get_popup().connect("id_pressed", self, "_level_select_on_item_pressed")
	button_start.connect("button_up", self, "_button_start_on_up")	
	for v in len(veh_selects):
		veh_selects[v].get_popup().connect("id_pressed", self, "_veh_select_player%d_on_item_pressed" % v)


func show():
	get_node("main_menu_tabs").show()
	get_node("logo").show()
	visible = true
	

func hide():
	get_node("main_menu_tabs").hide()
	get_node("logo").hide()
	visible = false


func is_visible():
	return visible


func _process(delta):
	if Input.is_action_pressed("ui_main_menu"):
		show()
		Game.hide_results_screen()
		find_node("vehicle_select_player0").grab_focus()
	l_num_players.set_text(String(Game.num_players))


func init_menu():
	""" Game Settings """
	
	print_debug("Preparing menu...")
	
	var label_info_join = find_node("label_info_join")
	label_info_join.set_text(tr("PRESS_BUTTON_TO_JOIN"))
	
	var level_select = find_node("level_select")

	var car_list = Content.get_cars()
	if len(car_list) == 0:
		Error.message(tr("ERROR_NO_CARS"))
		print(Settings.content_dir)
	var level_list = Content.get_levels()
	if len(level_list) == 0:
		Error.message(tr("ERROR_NO_LEVELS"))
		print(Settings.content_dir)
		
	for v in range(0, len(veh_selects)):
		var veh_select = veh_selects[v]
		veh_select.get_popup().clear()
		for car in car_list:
			veh_select.get_popup().add_item(car)

		veh_select.set_text(Settings.get("player%d" % v).vehicle)
	veh_selects[0].grab_focus()
	
	level_select.get_popup().clear()
	for level in level_list:
		level_select.get_popup().add_item(level)
	level_select.set_text(Settings.level)
	
	game_mode_select.get_popup().clear()
	for mode in Content.get_game_modes():
		game_mode_select.get_popup().add_item(mode)
	game_mode_select.set_text(Settings.game_mode)
	
	""" Settings """
	userpath_select.get_popup().clear()
	userpath_select.set_text(Settings.content_dir)
	for path in Settings.get_possible_content_dirs():
		userpath_select.get_popup().add_item(path)
		
	label_userpath.set_text("The path for the game settings file is at\n%s" % OS.get_user_data_dir())


func _veh_select_player0_on_item_pressed(item_id):
	_veh_select_on_item_pressed(item_id, 0)


func _veh_select_player1_on_item_pressed(item_id):
	_veh_select_on_item_pressed(item_id, 1)


func _veh_select_on_item_pressed(item_id, player_id):
	var veh_select = find_node("vehicle_select_player%d" % player_id)
	veh_select.set_text(veh_select.get_popup().get_item_text(item_id))
	Settings.get_player_settings(player_id).vehicle = veh_select.get_popup().get_item_text(item_id)


func _level_select_on_item_pressed(item_id):
	level_select.set_text(level_select.get_popup().get_item_text(item_id))
	Settings.level = level_select.get_popup().get_item_text(item_id)


func _userpath_select_on_item_pressed(item_id):
	var userpath_select = find_node("userpath_select")
	userpath_select.set_text(userpath_select.get_popup().get_item_text(item_id))
	Settings.content_dir = userpath_select.get_popup().get_item_text(item_id)
	init_menu()
	

func _game_mode_select_on_item_pressed(item_id):
	game_mode_select.set_text(game_mode_select.get_popup().get_item_text(item_id))
	Settings.game_mode = game_mode_select.get_popup().get_item_text(item_id)
	init_menu()


func _button_start_on_up():
	Settings.save_settings_file()
	get_node("/root/main/canvas_layer/main_ui/loading_screen").show()
	yield(get_tree(), "idle_frame")
	yield(get_tree(), "idle_frame")
	var success = Game.start()
	if success:
		hide()
	else:
		get_node("/root/main/canvas_layer/main_ui/loading_screen").hide()


""" Checks if an event is being sent from an unregistered controller and
	registers the device with a player 
"""
func register_check(event):
	if event is InputEventKey:
		if event.pressed and event.scancode == KEY_ENTER:
			if not Game.device_registered(-1):
				Game.register_player(-1)
				# Sets the input as handled so that it won't get picked up by the UI
				get_tree().set_input_as_handled()
	elif event is InputEventJoypadButton and event.pressed:
		if not Game.device_registered(event.get_device()):
			Game.register_player(event.get_device())
			get_tree().set_input_as_handled()


func _input(event):
	if is_visible():
		register_check(event)


func _userpath_select_custom_button_pressed():
	file_dialog.popup()


func _file_dialog_on_dir_selected(dir):
	Settings.content_dir = dir
	userpath_select.set_text(dir)
	init_menu()


func _unhandled_input(event):
	pass