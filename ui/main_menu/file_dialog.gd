extends FileDialog

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

var calling_node = null  #global in scene

func popup2(call_node):
  calling_node=call_node
  print("fileDialogue debug: called by node"+calling_node.name)
  popup()

func _on_FileDialog_file_selected(path):
  calling_node.get_image(path)