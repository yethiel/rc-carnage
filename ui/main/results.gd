extends Control

onready var label_title = get_node("panel/title")

var added_items = []

func _ready():
	hide()
	

""" Sets the results window title """
func set_title(title):
	label_title.set_text(title)


""" Sets the title of a results column """
func set_column_title(column_num, title):
	var column = get_node("panel/scroll_container/columns/column%d" % column_num)
	column.get_node("title").set_text(title)
	

""" Adds a text entry to a column """
func add_to_column(column_num, content):
	var column = get_node("panel/scroll_container/columns/column%d" % column_num)
	var label = Label.new()
	label.set_text(content)
	column.add_child(label)
	added_items.append(label)


func clear():
	for node in added_items:
		node.free()


func set_defaults():
	set_title("Results")
	set_column_title(0, "Place")
	add_to_column(0, "1st")
	add_to_column(0, "2nd")
	add_to_column(0, "9th")
	
	set_column_title(1, "Name")
	add_to_column(1, "URV")
	add_to_column(1, "Marv")
	add_to_column(1, "Huki")

	set_column_title(2, "Vehicle")
	add_to_column(2, "Clockwork Bus")
	add_to_column(2, "Rotor")
	add_to_column(2, "Volken Turbo")
	
	set_column_title(3, "Time")
	add_to_column(3, "00:10:34")
	add_to_column(3, "00:11:44")
	add_to_column(3, "00:11:45")
