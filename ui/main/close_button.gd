extends Button

func _pressed():
	var popup = get_parent()
	if popup.fatal:
		get_tree().quit()
	popup.hide()
	