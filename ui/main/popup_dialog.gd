extends PopupDialog

export var fatal = false

func show_message(message):
	popup_centered(Vector2(500, 200))
	get_node("label").set_text(message)
	if fatal:
		get_node("button").set_text("Quit Game")